import scrapy
from bs4 import BeautifulSoup


class SpiderConfigLoader:

    @staticmethod
    def load(path):
        config_file = open(path, 'r')
        return BeautifulSoup(config_file, features='xml').spider


class BlogSpider(scrapy.Spider):

    config_file = SpiderConfigLoader.load('./resources/spider_config.xml')

    name = config_file.config.name
    start_urls = [config_file.config.url.string]

    def parse(self, response):
        for title in response.css('.post-header>h2'):
            yield {'title': title.css('a ::text').extract_first()}

        for next_page in response.css('div.prev-post > a'):
            yield response.follow(next_page, self.parse)
